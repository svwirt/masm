TITLE Program Template     (project5.asm)

; Author: Stephanie Ayala
; Last Modified: 05-28-2018
; OSU email address: ayalas@oregonstate.edu
; Course number/section: CS271-400
; Project Number: #5             Due Date:05-27-2018
; Description: This progam gets a user input number in a certain range and generates a random array of numbers.
; It displays the numbers, sorts them in descending order, calculates the median, and displays the sorted array.
INCLUDE Irvine32.inc

MIN				=		 10
MAX				=		 200
LO				=		 100
HI				=		 999

.data
program		BYTE		"Sorting Random Integers ", 0
author		BYTE		"Programmed by Stephanie Ayala", 0
intro1		BYTE		"This program generates random numbers in the range [100 . . . 999]", 0
intro2		BYTE		"displays the original list, sorts the list, and calculates the median value.", 0
intro3		BYTE		"Finally, it displays the list sorted in decreacing order.", 0
prompt1		BYTE		"How many numbers should be generated? [10 . . .200]: ", 0
outRange	BYTE		"Out of range. Try again.", 0
unsorted	BYTE		"The unsorted random numbers:", 0
medString	BYTE		"The median is: ",0
sorted		BYTE		"the sorted list:", 0
spacePrint	BYTE		"   ", 0
count		DWORD		1
num			DWORD		?
requestTemp	DWORD		?
array		DWORD		MAX DUP(?)  

.code
 main PROC

	call	introduction
	push	OFFSET num
	call	getData
	push	OFFSET array
	push	num
	call	fillArray
	call	unsortedDisplay
	push	OFFSET array
	push	num
	call	displayList
	push	OFFSET array
	push	num
	call	sortList
	push	OFFSET array
	push	num
	call	displayMedian
	call	unsortedDisplay
	push	OFFSET array
	push	num
	call	displayList
	exit
main ENDP

; Description: This procedure introduces the user to the program
; Recieves: nothing
; Returns: nothing
; Preconditions: none
; Registers changed: edx

introduction PROC
	mov		edx, OFFSET program
	call	WriteString
	mov		edx, OFFSET author
	call	WriteString
	call	crlf
	mov		edx, OFFSET intro1
	call	WriteString
	call	crlf
	mov		edx, OFFSET intro2
	call	WriteString
	call	crlf
	mov		edx, OFFSET intro3
	call	WriteString
	call	crlf
	call	crlf
	ret
introduction ENDP

; Description: this procedure prompts the user for data and recieves it, then calls the validation procedure
; Recieves: userNum
; Returns: nothing
; Preconditions: none
;Registers changed: eax, edx

getData PROC
	push	ebp
	mov		ebp, esp
	mov		ebx, [ebp + 8]
top:
	mov		edx, OFFSET prompt1
	call	WriteString
	call	CrLf
	call    ReadInt
	mov     [ebx], eax			
	cmp		eax, MIN		;validate user data
	jb		outOfRange
	cmp		eax, MAX
	jg		outOfRange
	jmp		validated
outOfRange:
	mov		edx, OFFSET outRange
	call	WriteString
	call	CrLf
	jmp		top				;get new input if not valid
validated:
	call	crlf
	pop		ebp
	ret		4
getData ENDP

	
; Description: This procedure fills the array with random numbers
; Recieves: size of array- user entered
; Returns: none
; Preconditions: user number was validated
;Registers changed: eax, ecx, esi


fillArray PROC
	call	Randomize			; seed for random numbers
	push	ebp
	mov		ebp, esp
	mov		esi, [ebp + 12] 
	mov		ecx, [ebp + 8]  
RandomArray:
	mov		eax, HI
	sub		eax, LO
	inc		eax
	call	RandomRange
	add		eax, LO
	mov		[esi], eax			; save numbers in array
	add		esi, 4		
	loop	RandomArray
	pop		ebp
	ret		8
fillArray ENDP


; Description: This procedure displays the string for the unordered array
; Recieves: none
; Returns: none
; Preconditions: none
; Registers changed: edx

unsortedDisplay PROC
	mov		edx, OFFSET unsorted
	call	WriteString
	call	crlf
	ret
unsortedDisplay ENDP

; Description: This procedure prints the array to the screen
; Recieves: an array of integers
; Returns: none
; Preconditions: user number was validated
; Registers changed: eax, ecx, edx, ebx

displayList PROC
	push ebp
	mov  ebp, esp
	mov	 ebx, 0			 
	mov  esi, [ebp + 12]  
	mov	 ecx, [ebp + 8] 
printNext:
	mov		eax, [esi] 
	call	WriteDec
	mov		edx, OFFSET spacePrint
	call	WriteString
	mov		eax, count
	mov		ebx, 10		
	mov		edx, 0
	div		ebx
	cmp		edx, 0
	jne		continue			; if the output count has not reached 10 dont go to new line
	call	crlf
continue:
	add		esi, 4
	inc		count
	loop	printNext
	call	crlf
	pop		ebp
	ret		8
displayList ENDP

; Description: This sorts the unsorted array
; Recieves: an array of unsorted integers
; Returns: an aray of sorted integers
; Preconditions: none
; Registers changed: eax, ecx, edx, ebx

sortList PROC
	push ebp
	mov  ebp, esp
	mov	 ecx, [ebp + 8]				; loop control
	dec	 ecx					
outerLoop:
	mov		esi, [ebp + 12]
	mov		edx, ecx			
innerLoop:
	mov		eax, [esi]				; current element
	mov		ebx, [esi + 4]
	cmp		ebx, eax
	jle		noSwap					
	mov		[esi], ebx				; swap elements
	mov		[esi + 4], eax
noSwap:								; elements not swapped
	add		esi, 4					; next element
	loop	innerLoop
	mov		ecx, edx
	loop	outerLoop
	pop		ebp
	ret		8
sortList ENDP

; Description: This procedure finds and prints the median number of the array
; Recieves: an array of integers
; Returns: none
; Preconditions: user number was validated
; Registers changed: eax, ecx, edx, ebx

displayMedian PROC
	push ebp
	mov  ebp, esp
	mov  esi, [ebp + 12]  
	mov	 eax, [ebp + 8]
	mov  edx, 0
	mov	 ebx, 2
	div	 ebx					; check to see if there are an even or odd number of elements
	mov	 ecx, eax
L1:
	add		esi, 4
	loop	L1
	cmp		edx, 0				; if there is no remainder, then it is even
	jnz     medOfOdds
	call	medOfEvens
	jmp		exitMed
medOfOdds:
	mov		eax, [esi]
	mov		edx, OFFSET medString
	call	WriteString
	call	WriteDec
	call	CrLf
exitMed:
	pop  ebp
	ret  8
displayMedian ENDP

; Description: This procedure finds and prints the median number of the array if the number of elements is even
; Recieves: an even array of integers
; Returns: median
; Preconditions: user number was validated, array is even
; Registers changed: eax, edx, ebx

MedOfEvens PROC
	mov		eax, [esi-4]			; find middle two elements
	add		eax, [esi]
	mov		edx, 0
	mov		ebx, 2
	div		ebx						; mean of middle two elements
	mov		edx, OFFSET medString
	call	WriteString
	call	WriteDec
	call	crlf
	call	crlf
	ret
MedOfEvens	ENDP

; Description: This procedure displays the string for the sorted array
; Recieves: none
; Returns: none
; Preconditions: none
; Registers changed: edx

sortedDisplay	PROC
	mov		edx, OFFSET unsorted
	call	WriteString
	call	crlf
	ret
sortedDisplay	ENDP

END main