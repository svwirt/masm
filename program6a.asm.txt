TITLE lower level I/O procedures    (project6.asm)

; Author: Stephanie Ayala
; Last Modified: 06-07-2018
; OSU email address: ayalas@oregonstate.edu
; Course number/section: CS271-400
; Project Number: #6             Due Date:06-10-2018
; Description: This progam uses the macros getString and displayString to implement ReadVal and WriteVal
; for unsigned integers.  It tests these by getting 10 valid integers and storing them in array, summing, and averaging.

INCLUDE Irvine32.inc

NUM_INPUTS = 10

; Description: getString displays a prompt, then gets the user's keyboard input into a memory location
; Receives: prompt, string buffer, string length
; Returns: nothing
; Preconditions: no
; Registers changed: ecx, edx

getString	MACRO	prompting, buffer, sizeObuff
	push			edx
	push			ecx
	mov				edx, prompting		
	call			WriteString
	mov				edx, buffer
	mov				ecx, sizeObuff
	call			ReadString
	pop				ecx
	pop				edx
ENDM


; Description: displayString prints the string which is stored in a specific memory location
; Receives: string
; Returns: nothing
; Preconditions: string passed
; Registers changed: edx

displayString	MACRO	stringInput
	push			edx
	mov				edx, stringInput
	call			WriteString
	pop				edx
ENDM

.data
intro		BYTE	"PROGRAMMING ASSIGNMENT 6: Designing low-level I/O procedures written by: Stephanie Ayala", 0
instruct	BYTE	"Please provide 10 unsigned decimal integers.", 0
instruct1	BYTE	"Each number needs to be small enough to fit inside a 32 bit register.", 0
instruct2	BYTE	"After you have finished inputting the raw numbers I will display a list", 0
instruct3	BYTE	"of the integers, their sum, and their average value.", 0
userNum		BYTE	"Please enter an unsigned number: ", 0
error		BYTE	"ERROR: You did not enter an unsigned number or your number was too big.", 0
newNum		BYTE	"Please try again: ", 0
userArray	BYTE	"You entered the following numbers:", 0
spacing		BYTE	", ", 0
showSum		BYTE	"The sum of these numbers is: ", 0
showAvg		BYTE	"The average is: ", 0
goodbye		BYTE	"Thanks for playing!", 0
array		DWORD	10 DUP(?)
bool		DWORD	0


.code
main	PROC
	; introduce the program and give the user instructions
	push	OFFSET intro
	push	OFFSET instruct
	push	OFFSET instruct1
	push	OFFSET instruct2
	push	OFFSET instruct3
	call	introduction

	; get the user entered numbers
	push	OFFSET array
	push	LENGTHOF array
	push	OFFSET userNum
	push	OFFSET newNum
	push	OFFSET error
	call	getData
	
	; display the array of unsigned numbers
	push	OFFSET array
	push	LENGTHOF array
	push	OFFSET userArray
	push	OFFSET spacing
	call	displayList

	; display the sum/average
	push	OFFSET array
	push	LENGTHOF array
	push	OFFSET showSum
	push	OFFSET showAvg
	call	SumAndAvg

	; Goodby
	push	OFFSET goodbye
	call	thankYou

	exit
main	ENDP


; Description: introduces user to program
; Receives: intro and instruct
; Returns: none
; Preconditions: no
; Registers changed: edx, ebp

introduction		PROC	USES	edx
	push			ebp
	mov				ebp, esp
	mov				edx, [ebp + 28]		; display intro
	displayString	edx
	call			Crlf
	call			Crlf
	mov				edx, [ebp + 24]		; display instruct
	displayString	edx
	call			Crlf
	mov				edx, [ebp + 20]
	displayString	edx
	call			Crlf
	mov				edx, [ebp + 16]
	displayString	edx
	call			Crlf
	mov				edx, [ebp + 12]
	displayString	edx
	call			Crlf
	call			Crlf
	pop				ebp
	ret				8
introduction		ENDP


; Description: gets user array of unsigned ints. Uses readVal to get input, calls validate
; Receives: array, LENGTHOF array
; Returns: none
; Preconditions: values on stack
; Registers changed: esi, ecx, eax, ebp

getData		PROC	USES	esi ecx eax
	push			ebp
	mov				ebp, esp
	mov				esi, [ebp + 36]		
	mov				ecx, [ebp + 32]
arrayLoop:
	mov				eax, [ebp + 28]		; display userNum 
	push			eax
	push			[ebp + 24]			; newNum on stack
	push			[ebp + 20]			; error	on stack
	call			readVal
	pop				[esi]				; add num to array
	add				esi, 4
	loop			arrayLoop
	pop				ebp
	ret				20
getData			ENDP

; Description: readVal invokes getString macro to get user's string of digits and calls validate
; Receives: no
; Returns: validated user input
; Preconditions: no
; Registers changed: eax, ebx, ebp

readVal		PROC	USES	eax ebx
	LOCAL			num1[15]:BYTE
	push			esi
	push			ecx
	mov				eax, [ebp + 16]		; userNum 
	lea				ebx, num1
numsLoop:
	getString		eax, ebx, LENGTHOF num1
	mov				ebx, [ebp + 8]		; error on stack
	push			ebx
	lea				eax, bool
	push			eax
	lea				eax, num1
	push			eax
	push			LENGTHOF num1
	call			validate
	pop				edx
	mov				[ebp + 16], edx		; converted ints 
	mov				eax, bool			
	cmp				eax, 1
	mov				eax, [ebp + 12]
	lea				ebx, num1
	jne				numsLoop
	pop				ecx
	pop				esi
	ret				8	
readVal		ENDP


; Description: this procedure validates the user input numbers and calls the convert PROC
; Receives: user input numbers
; Returns: valid numbers
; Preconditions:nums on stack to be validated
; Registers changed: esi, ecx, eax, edx, ebp

validate	PROC	USES	esi ecx eax edx
	LOCAL			bigNum:DWORD
	mov				esi, [ebp + 12]		; loop counter
	mov				ecx, [ebp + 8]
	cld
stringLoop:
	lodsb
	cmp				al, 0
	je				badChar
	cmp				al, 48
	jnge			invalid
	cmp				al, 57
	jnbe			invalid
	loop			stringLoop
invalid:
	mov				edx, [ebp + 20]		; display error 
	displayString	edx
	call			Crlf
	mov				edx, [ebp + 16]		; bool = false
	mov				eax, 0
	mov				[edx], eax
	jmp				validated
badChar:
	mov				edx, [ebp + 8]	
	cmp				ecx, edx	
	je				invalid				; null was entered
	lea				eax, bigNum
	mov				edx, 0
	mov				[eax], edx
	push			[ebp + 12]
	push			[ebp + 8]
	lea				edx, bigNum
	push			edx
	call			stringToInt
	mov				edx, bigNum
	cmp				edx, 1
	je				invalid
	mov				edx, [ebp + 16]
	mov				eax, 1				; bool = true
	mov				[edx], eax
validated:
	pop				edx	
	mov				[ebp + 20], edx		; converted number
	ret				12		
validate	ENDP


; Description: this proc reads a string and converts it into a valid number 

; Receives: numstring and size
; Returns: converted int
; Preconditions: validated string of digits
; Registers changed: esi, ecx, eax, ebx, edx, ebp

stringToInt		PROC	USES	esi ecx eax ebx edx
	LOCAL			tempNum:DWORD
	mov				esi, [ebp + 16]
	mov				ecx, [ebp + 12]
	lea				eax, tempNum
	xor				ebx, ebx
	mov				[eax], ebx
	xor				eax, eax
	xor				edx, eax	
	cld
loadNums:
	lodsb
	cmp				eax, 0
	je				endLoop
	sub				eax, 48
	mov				ebx, eax
	mov				eax, tempNum
	mov				edx, NUM_INPUTS
	mul				edx
	jc				farTooBig				; too big if carry flag is set
	add				eax, ebx
	jc				farTooBig	
	mov				tempNum, eax	
	mov				eax, 0		
	loop			loadNums
endLoop:
	mov				eax, tempNum
	mov				[ebp + 16], eax			; move converted number to memory
	jmp				endConvert
farTooBig:									; if num cant fit in 32 reg
	mov				ebx, [ebp + 8]			
	mov				eax, 1					; farTooBig = true
	mov				[ebx], eax
	mov				eax, 0
	mov				[ebp + 16], eax			
endConvert:
	ret	8
stringToInt		ENDP

; Description: prints the string that is stored in a specific memory location
; Receives: array and size of the array
; Returns: none
; Preconditions: validated values on stack
; Registers changed: esi, ecx, ebx, edx, ebp

displayList	PROC	USES	esi ebx ecx edx
	push			ebp
	mov				ebp, esp
	call			Crlf
	mov				edx, [ebp + 28]		; userArray
	displayString	edx
	call			Crlf
	mov				esi, [ebp + 36]
	mov				ecx, [ebp + 32]
	mov				ebx, 1				; row counter
ShowNum:
	push			[esi]
	call			WriteVal
	add				esi, 4
	cmp				ebx, [ebp + 32]
	jnl				noComma	
	mov				edx, [ebp + 24]		; display space
	displayString	edx
	inc				ebx
	loop			ShowNum
noComma:
	call			Crlf
	pop				ebp
	ret				16
displayList	ENDP


; Description: calculates and show the sum ans average of the array
; Receives: array, array size
; Returns: none
; Preconditions: validated numbes on stack
; Registers changed: edx, esi, ecx, eax, ebx, ebp

SumAndAvg	PROC	USES	esi	edx ecx eax ebx
	push			ebp
	mov				ebp, esp
	mov				edx, [ebp + 32]		; display showSum
	displayString	edx
	mov				esi, [ebp + 40]		
	mov				ecx, [ebp + 36]		
	xor				eax, eax	
addArray:
	add				eax, [esi]			; get the sum
	add				esi, 4
	loop			addArray
	push			eax
	call			writeVal
	call			Crlf
	mov				edx, [ebp + 28]		; display showAvg
	displayString	edx
	cdq
	mov				ebx, [ebp + 36]		; array size
	div				ebx
	push			eax	
	call			writeVal			; show avg
	call			Crlf
	pop				ebp
	ret				16
SumAndAvg	ENDP

; Description: converts a numeric value to a string of digits and invokes displayString macro
; Writes an integer to the console as a string. Calls on convertChar to convert
; Receives: int
; Returns: none
; Preconditions: validated int on stack
; Registers changed: eax, ebp

writeVal	PROC	USES	eax
	LOCAL			outputStr[11]:BYTE
	lea				eax, outputStr
	push			eax
	push			[ebp + 8]			; num on stack
	call			converter
	lea	eax,		outputStr
	displayString	eax
	ret				4
writeVal		ENDP

; Description: this proc converts the ints to a string 
; Receives: user entered value
; Returns: none
; Preconditions: valadated user entered num
; Registers changed: eax, ebx, ecx, ebp

converter	PROC	USES	eax ebx ecx
	LOCAL			temp:DWORD
	mov				eax, [ebp + 8]
	mov				ebx, NUM_INPUTS
	mov				ecx, 0
	cld
divLoop:
	cdq
	div				ebx					; divides ints by 10
	push			edx					; push remainder
	inc				ecx
	cmp				eax, 0
	jne				divLoop
	mov				edi, [ebp + 12]		; array for string
addString:
	pop				temp
	mov				al, BYTE PTR temp
	add				al, 48
	stosb
	loop			addString
	mov				al, 0
	stosb
	ret				8
converter		ENDP

; Description: goodbye/thanks message
; Receives: no
; Returns: none
; Preconditions: no
; Registers changed: ebp, edx

thankYou	PROC	USES	edx
	push			ebp
	mov				ebp, esp
	call			Crlf
	mov				edx, [ebp + 12]		; display goodbye 
	displayString	edx
	call			Crlf
	pop				ebp
	ret				4
thankYou	ENDP

END	main